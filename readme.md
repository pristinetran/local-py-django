# Python3 pip install (similar to npm install) before launching this repo:

In a terminal pointing to this project directory, use `. ./virtual_env` OR `source virtualenv_exec/bin/activate` to open virtual env

In the virtual env, run the following cli:

`sudo -H pip3 install -r requirements3.txt`


# Python3 Installation folder - MAC

['/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/django']

# Run the app

1/ with `. ./start3`

OR 2/

sampsite $ python3 ./manage.py runserver

If python3 doesn't have Django install, you can use Python2.7 `sampsite $ python3 ./manage.py runserver` OR perform `pip3 install django` to install Django with `pip3`

For TESTING: `. ./test` OR `python3 manage.py test polls`

json.dumps : serialize (encode)

json.loads : deserialize (decode)

More read: https://docs.python.org/2/library/json.html

***1.4 Persistence with sqlite3***

add PollsConfig to INSTALLED_APP

makemigration cli: `./manage.py makemigrations polls`

sqlmigrate cli: `./manage.py sqlmigrate polls 0001`

To CREATE the DB: `./manage.py migrate`

add data to our database: ./manage.py shell

`>>> from polls.models import Question, Choice`

Display all the questions: >>> Question.objects.all()

CREATE a new RECORD 

`>>> from django.utils import timezone`

`>>> q = Question(question_text = "What's New?", pub_date=timezone.now())`

`>>> q.save()`

`>>> q.question_text`

`>>> q.pub_date`

`>>> q.id`

Change the question: >>> q.question_text = "What's Up?"

Save the change: >>> q.save()

*** FILTER search ***

`>>> Question.objects.filter(id=1)`

`>>> Question.objects.filter(question_text__startswith='What')`

`>>> from django.utils import timezone`

`>>> current_year = timezone.now().year`

`>>> Question.objects.get(pub_date__year=current_year)`

`>>> Question.objects.get(pk=1)`

`>>> q = Question.objects.get(pk=1)`

`>>> q.was_published_recently()`

`>>> q.choice_set.all()`

`<QuerySet []>`

`>>> q.choice_set.create(choice_text = 'Not Much', votes = 0)`

`<Choice: Not Much>`

`>>> q.choice_set.create(choice_text = 'The Sky', votes = 0)`

`<Choice: The Sky>`

`>>> q.choice_set.create(choice_text = 'The Cloud', votes = 0)`

`<Choice: The Cloud>`

`>>> q.choice_set.all()`

`<QuerySet [<Choice: Not Much>, <Choice: The Sky>, <Choice: The Cloud>]>`

`>>> q.choice_set.count() 		// 3`

`>>> Choice.objects.filter(question__pub_date__year=current_year)`

`<QuerySet [<Choice: Not Much>, <Choice: The Sky>, <Choice: The Cloud>]>`

`>>> c = q.choice_set.filter(choice_text__startswith='The Cloud')`

`>>> c.delete()  		// delete that choice`

`>>> Choice.objects.filter(question__pub_date__year=current_year)  		// review`

*** ADMIN Django app ***

`./manage.py createsuperuser`

`{ admin, ul***rine}`


*** Django TESTING ***

`>>> import datetime`

`>>> from django.utils import timezone`

`>>> from polls.models import Question`

`>>> future_question = Question(pub_date=timezone.now()+datetime.timedelta(days=30))`

`>>> future_question.was_published_recently()`

True!!!


*** Django E2E testing ***

`>>> from django.test.utils import setup_test_environment`

`>>> setup_test_environment()`

`>>> from django.test import Client`

`>>> client = Client()`

`>>> response = client.get('/')`

`>>> response.status_code   // 200`

`>>> from django.urls import reverse`

`>>> response = client.get(reverse('polls:index'))`

`>>> response.status_code   // 200`

`>>> response.content`

`'\n\t<ul>\n\t\t\n\t\t<li>\n\t\t\t<a href="/polls/2/">capital of Finland?</a>\n\t\t</li>\n\t\t\n\t\t<li>\n\t\t\t<a href="/polls/1/">What&#39;s Up?</a>\n\t\t</li>\n\t\t\n\t</ul>\n'`

`>>> response.context['latest_question_list']`

`<QuerySet [<Question: capital of Finland?>, <Question: What's Up?>]>`

Location of base_site.html for Django Administration page - change header of the AdminCP site

/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/django/contrib/admin/templates/admin/base_site.html




